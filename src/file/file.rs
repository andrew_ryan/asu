use std::fs;
pub use std::{
    fmt::Display,
    fs::{create_dir_all, write},
    path::Path,
    process,
};

use doe::{split_to_vec, vec_element_clone};

use crate::net::net;
use crate::toml::toml;

pub fn read_config_file() {
    let mut dir = dirs::home_dir().expect("not fond home_dir");
    dir.push(".asu.toml");
    let is_file = dir.exists();
    if is_file {
        let config =
            String::from_utf8_lossy(&fs::read(dir).expect("read config_file error")).to_string();
        let mut toml_data = toml::Toml::parse(&config).expect("parse err");
        let table = toml_data.table_mut();
        let mirror = table.get("mirror").expect("not found mirror table");
        let vec = mirror
            .as_table()
            .expect("as_table error")
            .get("vec")
            .expect("not found vec table")
            .as_value()
            .expect("as_value error");
        let vec_t = vec
            .as_inline_table()
            .expect("as_inline_table error")
            .get("urls")
            .expect("get urls err");
        let mut vec_mirrors = vec![];
        let arr = vec_t.as_array().expect("as_array error");
        for i in arr.iter() {
            vec_mirrors.push(i.to_string().trim().replace("\"", ""));
        }
        // println!("{:?}", vec_mirrors);
        let req = net::request_result(vec_mirrors);
        if req.len() > 0 {
            let first = req.first().expect("not find first");
            let url = &first.0;
            let _data = format!("SigLevel=Never\nServer = {}", url);
            write_mirrorlist(_data);
        }
    }
}

pub fn read_config_file_not_write() {
    let mut dir = dirs::home_dir().expect("not fond home_dir");
    dir.push(".asu.toml");
    let is_file = dir.exists();
    if is_file {
        let config =
            String::from_utf8_lossy(&fs::read(dir).expect("read config_file error")).to_string();
        let mut toml_data = toml::Toml::parse(&config).expect("parse err");
        let table = toml_data.table_mut();
        let mirror = table.get("mirror").expect("not found mirror table");
        let vec = mirror
            .as_table()
            .expect("as_table error")
            .get("vec")
            .expect("not found vec table")
            .as_value()
            .expect("as_value error");
        let vec_t = vec
            .as_inline_table()
            .expect("as_inline_table error")
            .get("urls")
            .expect("get urls err");
        let mut vec_mirrors = vec![];
        let arr = vec_t.as_array().expect("as_array error");
        for i in arr.iter() {
            vec_mirrors.push(i.to_string().trim().replace("\"", ""));
        }
        // println!("{:?}", vec_mirrors);
        let req = net::request_result(vec_mirrors);
        if req.len() > 0 {
            let first = req.first().expect("not find first");
            let url = &first.0;
            let _data = format!("SigLevel=Never\nServer = {}", url);
            // write_mirrorlist(_data);
        }
    }
}

pub fn create_new_config_file(country_name: &str) -> Result<bool, String> {
    let mut dir = dirs::home_dir().expect("not fond home_dir");
    dir.push(".asu.toml");
    println!("now create .asu.toml file");
    let mut toml_front = r#"[mirror]
vec={urls="#
        .as_bytes()
        .to_vec();
    toml_front.extend(format!("{:?}", net::get_arch_repo(&country_name)).as_bytes());
    toml_front.push('}' as u8);
    toml_front.extend(format!("\n[country]\nname={:?}", &country_name).as_bytes());
    fs::write(dir, toml_front).expect("config_file write failed");
    Ok(true)
}

pub fn create_config_file() -> Result<bool, String> {
    let mut dir = dirs::home_dir().expect("not fond home_dir");
    dir.push(".asu.toml");
    let is_file = dir.exists();
    if !is_file {
        if doe::args!().len() == 1 && all_uppercase(doe::vec_element_clone!(doe::args!(), 0)) {
            println!("now create .asu.toml file");
            let mut toml_front = r#"[mirror]
vec={urls="#
                .as_bytes()
                .to_vec();
            toml_front.extend(format!("{:?}", net::get_arch_repo(&doe::args!()[0])).as_bytes());
            toml_front.push('}' as u8);
            toml_front.extend(format!("\n[country]\nname={:?}", &doe::args!()[0]).as_bytes());
            fs::write(dir, toml_front).expect("config_file write failed");
        } else {
            println!(
                "The first run requires a 2 capital letter country abbreviated name\n
#example
sudo asu -n FR => France mirror source
sudo asu -n US => United States mirror source
sudo asu -n AU => Australia mirror source
sudo asu -n DE => Germany mirror source
sudo asu -n GB => British mirror source
sudo asu -n CN => China mirror source
#If you are not sure run 'asu -c' to see the abbreviated name of your country
"
            );
        }
    }
    Ok(true)
}
pub fn write_mirrorlist(msg: impl ToString) {
    // /etc/pacman.d/mirrorlist
    println!("{}", msg.to_string());
    fs::write("/etc/pacman.d/mirrorlist", msg.to_string()).expect("write mirrorlist failed");
}

pub fn all_uppercase(s: impl ToString) -> bool {
    let ss = s.to_string();
    let mut re = true;
    for s in ss.as_bytes().to_vec().iter() {
        if s < &65 || s > &90 {
            re = false;
            break;
        }
    }
    re
}
pub fn use_mirror(name: impl ToString) {
    let mirrors = get_mirror_list();
    let url_vec = mirrors
        .iter()
        .filter(|s| s.contains(&name.to_string()))
        .collect::<Vec<_>>();
    if url_vec.len() > 0 {
        let _data = format!("SigLevel=Never\nServer = {}", url_vec[0]);
        write_mirrorlist(_data);
    }
}
pub fn get_mirror_list() -> Vec<String> {
    let mut dir = dirs::home_dir().expect("not fond home_dir");
    dir.push(".asu.toml");
    let is_file = dir.exists();
    let mut re = vec![];
    if is_file {
        let config =
            String::from_utf8_lossy(&fs::read(dir).expect("read config_file error")).to_string();
        let mut toml_data = toml::Toml::parse(&config).expect("parse err");
        let table = toml_data.table_mut();
        let mirror = table.get("mirror").expect("not found mirror table");
        let vec = mirror
            .as_table()
            .expect("as_table error")
            .get("vec")
            .expect("not found vec table")
            .as_value()
            .expect("as_value error");
        let vec_t = vec
            .as_inline_table()
            .expect("as_inline_table error")
            .get("urls")
            .expect("get urls err");
        let mut vec_mirrors = vec![];
        let arr = vec_t.as_array().expect("as_array error");
        for i in arr.iter() {
            vec_mirrors.push(i.to_string().trim().replace("\"", ""));
        }
        re = vec_mirrors;
    }
    re
}
pub fn list_mirror() {
    println!("name\t\turl");
    for url in get_mirror_list().iter() {
        let name = vec_element_clone!(split_to_vec!(url, "."), 1);
        println!("{}\t\t{}", name, url);
    }
}
