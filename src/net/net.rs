use std::time::{Duration, SystemTime};
pub fn request(url: impl ToString + std::fmt::Debug + std::fmt::Display) -> Option<(u128, f64)> {
    let time = SystemTime::now();
    match ureq::get(&format!(
        "{}",
        url.to_string().split("$repo").collect::<Vec<_>>()[0]
    ))
    .timeout(Duration::from_secs(10))
    .call()
    {
        Ok(res) => {
            let status = res.status();
            if status >= 400 {
                return None;
            }
            if status >= 300 {
                return match res.header("location") {
                    Some(v) => request(&v.to_string()),
                    None => None,
                };
            }
            Some((
                time.elapsed().unwrap().as_millis(),
                time.elapsed().unwrap().as_millis().pow(1) as f64 / 1000.,
            ))
        }
        Err(_) => None,
    }
}
#[allow(warnings)]
pub fn request_result(mirror_vec: Vec<String>) -> Vec<(String, Option<(u128, f64)>)> {
    // let mirror_vec = crate::constants::mirror_vec();
    let mut re = vec![];
    for url in mirror_vec {
        let rev = request(url.to_string());
        match rev {
            Some(v) => {
                re.push((url.to_string(), rev));
            }
            None => {}
        }
        match rev {
            Some(r) => {
                println!("{},{}ms,{}s", url.to_string(), r.0, r.1);
            }
            None => {
                println!("{} timeout", url.to_string());
            }
        }
    }
    re.sort_by(|a, b| {
        let aa = a.1.expect("timeout");
        let bb = b.1.expect("timeout");
        aa.0.cmp(&bb.0)
    });
    re
}

pub fn get_arch_repo(country_name: impl ToString) -> Vec<String> {
    // "https://archlinux.org/mirrorlist/?country=CN&protocol=https&use_mirror_status=on"

    let response = reqwest::blocking::get(&format!("https://archlinux.org/mirrorlist/?country={}&protocol=http&ip_version=4&ip_version=6&use_mirror_status=on",country_name.to_string())).unwrap();
    // println!("{:?}", response.headers);
    println!(
        "url:https://archlinux.org/mirrorlist/?country={}&protocol=http&use_mirror_status=on",
        country_name.to_string()
    );
    println!("status:{}", response.status().as_str());
    if response.status().as_str() == "200" {
        let repo = response.text().unwrap();
        let repo_vec = repo
            .split("\n")
            .map(|s| s.trim().to_string())
            .filter(|s| s.contains("http"))
            .map(|s| s.replace("#Server = ", ""))
            .collect::<Vec<_>>();
        return repo_vec;
    } else {
        vec![]
    }
}
