use doe::vec_element_clone;

use crate::data::data;
use crate::file::file;

pub fn run() {
    let arg = doe::args!();
    if arg.len() > 0 && (arg[0] == "-h" || arg[0] == "-help") {
        println!(
            "asu - archlinux set mirror source cli
Usage:        
asu -n FR|| -new FR => fetch archlinux France mirror source and set fatest pacman mirror
asu -u name|| -use name => set selected mirror source
asu -l || -list => list all mirror list
asu -w || -where => shows the .asu.toml file path
asu -c || -country => shows all country abbreviated names 
asu -t || -test => get config from local .asu.toml file and test speed
asu -b || -best => get config from local .asu.toml file and set best pacman mirror
"
        );
    } else if arg.len() > 0 && (arg[0] == "-c" || arg[0] == "-country") {
        println!("{}", data::data::all_country())
    } else if arg.len() > 0 && (arg[0] == "-w" || arg[0] == "-where") {
        let mut dir = dirs::home_dir().expect("not fond home_dir");
        dir.push(".asu.toml");
        println!("{:?}", dir);
    } else if arg.len() > 0 && (arg[0] == "-l" || arg[0] == "-list") {
        match file::create_config_file() {
            Ok(_r) => {
                file::list_mirror();
            }
            Err(err) => println!("Error creating config file: {}", err),
        }
    } else if arg.len() > 1 && (arg[0] == "-u" || arg[0] == "-use") {
        if arg[1] == "-h" || arg[1] == "-help" {
            println!("asu -l =>list all mirror source and mirror names\nasu -u mirror_name =>set slected mirror for pacman
            ");
        }
        match file::create_config_file() {
            Ok(_r) => {
                file::use_mirror(vec_element_clone!(arg, 1));
            }
            Err(err) => println!("Error creating config file: {}", err),
        }
    } else if arg.len() > 0 && (arg[0] == "-b" || arg[0] == "-best") {
        match file::create_config_file() {
            Ok(_r) => {
                file::read_config_file();
            }
            Err(err) => println!("Error creating config file: {}", err),
        }
    } else if arg.len() > 0 && (arg[0] == "-t" || arg[0] == "-test") {
        match file::create_config_file() {
            Ok(_r) => {
                file::read_config_file_not_write();
            }
            Err(err) => println!("Error creating config file: {}", err),
        }
    } else if arg.len() > 1 && (arg[0] == "-n" || arg[0] == "-new") {
        match file::create_new_config_file(&arg[1]) {
            Ok(_r) => {
                file::read_config_file();
            }
            Err(err) => println!("Error creating config file: {}", err),
        }
    } else {
        println!(
            "asu - archlinux set mirror source cli
Usage:        
asu -n FR|| -new FR => fetch archlinux France mirror source and set fatest pacman mirror
asu -u name|| -use name => set selected mirror source
asu -l || -list => list all mirror list
asu -w || -where => shows the .asu.toml file path
asu -c || -country => shows all country abbreviated names 
asu -t || -test => get config from local .asu.toml file and test speed
asu -b || -best => get config from local .asu.toml file and set best pacman mirror
"
        );
    }
}
