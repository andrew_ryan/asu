# Archlinux set the fastest mirror source cli

[![Crates.io](https://img.shields.io/crates/v/asu.svg)](https://crates.io/crates/asu)
[![Rust](https://img.shields.io/badge/rust-1.56.1%2B-blue.svg?maxAge=3600)](https://gitlab.com/andrew_ryan/asu)
[![license](https://img.shields.io/badge/license-MIT-blue.svg)](https://gitlab.com/andrew_ryan/asu/-/raw/master/LICENSE)

## Examples

```
asu -h
```

```
asu - archlinux set mirror source cli
Usage:        
asu -n FR|| -new FR => fetch archlinux France mirror source and set fatest pacman mirror
asu -u name|| -use name => set selected mirror source
asu -l || -list => list all mirror list
asu -w || -where => shows the .asu.toml file path
asu -c || -country => shows all country abbreviated names 
asu -t || -test => get config from local .asu.toml file and test speed
asu -b || -best => get config from local .asu.toml file and set best pacman mirror
```

## Install

```
cargo install asu
```

## Run to set

```
sudo asu
```

```
The first run requires a 2 capital letter country abbreviated name
#example
sudo asu -n FR => France mirror source
sudo asu -n AU => Australia mirror source
sudo asu -n US => Us mirror source
sudo asu -n DE => Germany mirror source
sudo asu -n GB => British mirror source
sudo asu -n CN => China mirror source
```
